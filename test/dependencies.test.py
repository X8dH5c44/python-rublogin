from selenium import webdriver
from pyvirtualdisplay import Display
from urllib import request

url = 'https://duckduckgo.com'

try:
  request.urlopen(url)
  print('testing url reachable...')
except:
  print('testing url unreachable. aborting test...')
  exit(1)

display = Display(visible=0, size=(1920, 1080))
print('starting virtual display...')
display.start()

print('getting chromium webdriver...')
driver = webdriver.Chrome()
print(f'get: {url}...')
driver.get(url)
print(f'saving screenshot...')
driver.save_screenshot('screenshot.png')
