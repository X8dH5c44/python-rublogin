from selenium import webdriver
from selenium.webdriver.common.by import By
from pyvirtualdisplay import Display
from configparser import ConfigParser
import urllib.request
import time
import logging as log

def main():
  log.basicConfig(level=log.DEBUG)
  (user, passwd, loginurl, wwwurl) = config()
  while True:
    if check_network(loginurl):
      log.info('Login page found.')
      if check_network(wwwurl):
        log.info("Already logged in.")
      else:
        log.info('Logging in...')
        login(loginurl, user, passwd)
        log.info('Logged in.')
    time.sleep(60)

def login(loginurl, user, passwd):
  page = get_page(loginurl)
  loginid = page.find_element(By.NAME, "loginid")
  password = page.find_element(By.NAME, "password")
  button = page.find_element(By.NAME, "action")
  loginid.send_keys(user)
  password.send_keys(passwd)
  button.click()

def get_page(loginurl):
  vdisplay()
  log.debug('Getting Chrome webdriver...')
  driver = webdriver.Chrome()
  log.info('Loading login page...')
  driver.get(loginurl)
  return driver

def vdisplay():
  display = Display(visible=0, size=(1920, 1080))
  log.debug('Starting virtual display...')
  display.start()

def check_network(host):
  try:
    urllib.request.urlopen(host, timeout=5)
    return True
  except:
    return False 

def load_config(file):
  log.debug(f'Loading {file}.ini...')
  ini = ConfigParser()
  ini.read(file + '.ini')
  return ini

def read_config(config, section, value):
  try:
    log.debug(f'Getting {value} from {section}...')
    return config.get(section, value)
  except:
    log.error(f'Config parameter {value} not found in {section}.')
    log.error('Aborting.')
    exit(1)

def config():
  credentials = load_config('credentials')
  config = load_config('config')
  return (
    read_config(credentials, 'credentials', 'user'),
    read_config(credentials, 'credentials', 'passwd'),
    read_config(config, 'url', 'login'),
    read_config(config, 'url', 'www'),
  )

main()
