#!/bin/sh

if [ -f credentials.ini ]; then
  echo 'credentials.ini already exists.'
else
  echo 'creating fresh credentials.ini...'
  echo '[credentials]' > credentials.ini
  echo 'user=' >> credentials.ini
  echo 'passwd=' >> credentials.ini
fi

