# Python RUB-Login (HiRN)
This program can automatically log you into the RUB HiRN network,
if all dependencies are present.

## Setup
To set up this program on a Unix machine, run:
```
sudo chmod +x ./install.sh
./install.sh
```

Enter your credentials into the newly created `credentials.ini`.

Then install the dependencies listed under [Dependencies](#dependencies) using `pip3`
and your distribution's package manager.

### Dependencies
#### System Dependencies
You will need:
- A Python interpreter; likely called `python3`
- The Pip3 package manager; likely called `python3-pip`
- The Chromium browser; likely called `chromium` or `chromium-browser`
- The Chromium webdriver; likely called `chromium-chromedriver`

#### Python Dependencies
Install the required Python packages using:
```
pip3 install selenium pyvirtualdisplay
```

**You should now be able to run:** `python3 rub.py`.

## Installation as a service
To set up the program to be automatically executed,
have your init system run `python3 rub.py` on boot.

Your init system will most likely be called `systemd`.
For other init systems,
please see their respective documentation about creating
and activating service files.

### systemd
Create a file called `rublogin.service` under `/etc/systemd/system/`
and paste these contents into it:
```
[Unit]
Description=Automatic Login for RUB HiRN
After=multi-user.target

[Service]
ExecStart=/usr/bin/python3 <PATH>
Type=simple

[Install]
WantedBy=multi-user.target
```

where you replace `<PATH>` with the path pointing to `rub.py`.

You can now enable the service for your next reboot with:
```
sudo systemctl daemon-reload
sudo systemctl enable rublogin
```

and/or start it directly with:
```
sudo systemctl start rublogin
```

after reloading `systemd`'s daemon configuration like above.










